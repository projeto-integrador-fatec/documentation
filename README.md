**Team Members**

- [ ] Bruno Ferro - PO
- [ ] Fabio Rodrigues - FullStack Developer
- [ ] Leonardo Tamanhão - IA Developer
- [ ] Luciano Cabral - Scrum Master
- [ ] Luis Belo - Web Developer
- [ ] Matheus Froes - Backend Developer

## O que é?

O projeto visa criar uma plataforma de análise de imagens de satélite através de
um serviço de Inteligencia Artificial (IA) capaz de identificar talhões e disponibilizar
um catálogo de imagens para o usuário realizar download de sua busca.

This project aims to create a plataform for analysis of satellite image by means of AI to
making it able to identify plots of land and provide a image catalog where the user can download
his research.

## Tecnologias utilizadas:

**[Imgur API](https://apidocs.imgur.com/?version=latest)** To store and provide images
for the WebGIS plataform.

**[MongoDB](https://docs.mongodb.com/manual//)** NoSQL Database to store users informations and
allowing requests queries to be run.

**[Keras](https://keras.io/) [Tensor Flow](https://www.tensorflow.org/)** Neural network
that will be the core of the project written in Python and capable of running on top of
TensorFlow.

**[WebGIS](http://www.webgis.com/) with [VueJS](https://vuejs.org/)** To develop the frontend 
of the platform.

**[Mapbox](https://www.mapbox.com/)** For the Map Tile Engine os the plataform.

**[Flask](https://palletsprojects.com/p/flask/)** and
**[GeoPandas](https://geopandas.org/)** To develop backend tools such as CRUD.

## Canvas of Project:

![Screenshot](Canvas.PNG)

## Planejamento de Custos e Horas:

https://drive.google.com/open?id=1b351IJFYBzuri6sx5ZlnqnobeXm4cSlS

## Sprint 1:

Objetivo: Pesquisar sobre o tema proposto, sobre as ferramentas que serão utilizadas na resolução do problema e 
desenvolver a primeira página da plataforma com a visualização do mapa e uma ferramenta básica de seleção

**[Documentation](https://gitlab.com/projeto-integrador-fatec/documentation/-/tree/Sprint_1)** 

**[Backlog](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/backlog/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%201)** 

**[Burndown Trend](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/analytics/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%201)** 


## Sprint 2:

Objetivo: Criar um modelo simples de IA de classificação de imagens, desenvolver uma API de inferface para o 
WebGIS e um banco de dados para o armazenamento das máscaras geradas pelo serviço de IA.

**[Documentation](https://gitlab.com/projeto-integrador-fatec/documentation/-/tree/Sprint_2)** 

**[Backlog](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/backlog/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%202)** 

**[Burndown Trend](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/analytics/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%202)** 

**[Vídeo Explicativo](https://drive.google.com/file/d/1PdccgYjTxWIP8tYIAMJokku05IiwubY8/view)**

## Sprint 3:

Objetivo: Atualizar o modelo de IA para imagens de satélite, contrução do Catálogo de Imagens no WebGIS, simplificação da API e do banco de dados
e incorporação da API do Imgur como forma de armazenamento e acesso as imagens geradas pelo serviço de IA.

**[Documentation](https://gitlab.com/projeto-integrador-fatec/documentation/-/tree/Sprint_3)** 

**[Backlog](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/backlog/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%203)** 

**[Burndown Trend](https://dev.azure.com/lucianocruz01/Projeto%20Integrador/_sprints/analytics/Projeto%20Integrador%20Team/Projeto%20Integrador/Sprint%203)** 

**[Vídeo Explicativo](https://drive.google.com/file/d/13XdghdyzcxnF0Pk03Qyz9ir4PzfKkvkz/view)**
